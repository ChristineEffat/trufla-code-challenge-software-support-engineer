function stack( stackOperation, stackValue ) {
	 var stackHolder = {
	count: 4,
	storage : [
	  1,
	  '{id: 1,value: "obj"}',
	  "stringHolder",
	  46
	]
  };

  var push = function(value) {
	stackHolder[storage.length] = stackValue;
	// or use push...
	//stackHolder.storage.push(value);
	stackHolder.count ++;
	return stackHolder.storage;
  }

  var pop = function() {
    if (stackHolder.count === 0) {
	  return [];
    }

    var poppedItem = stackHolder.storage[stackHolder.count - 1];
    stackHolder.storage.pop();
		stackHolder.count--;
    return poppedItem;
  }

  var peek = function() {
    return stackHolder.storage[stackHolder.count - 1];
  }

  var swap = function() {
	var lastItem = stackHolder.storage[stackHolder.count - 1];
	stackHolder.storage[stackHolder.count - 1] = stackHolder.storage[stackHolder.count - 2];
	stackHolder.storage[stackHolder.count - 2] = lastItem;
	return stackHolder.storage;
  }

  switch(stackOperation) {
	case 'push':
	  push(stackValue);
	break;
	case 'pop':
	  pop();
	break;
	case 'swap':
	  swap();
	break;
	case 'peek':
	  peek();
	break;
	default:
	  return stackHolder.storage;
  }
}
