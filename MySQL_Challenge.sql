SELECT country.name, count(*) as employees
FROM employee 
INNER Join country ON (country.id = employee.Country_ID )
INNER Join working_center ON ( country.id = working_center.Country_ID)
INNER Join employee_working_center ON (employee.id = employee_working_center.employee_id)
where working_center.id = employee_working_center.working_center_id
group by country.name
order by employees desc, country.name;